#include "MOESIF_protocol.h"
#include "../sim/mreq.h"
#include "../sim/sim.h"
#include "../sim/hash_table.h"

extern Simulator *Sim;

/*************************
 * Constructor/Destructor.
 *************************/
MOESIF_protocol::MOESIF_protocol (Hash_table *my_table, Hash_entry *my_entry)
  : Protocol (my_table, my_entry)
{
  this->state = MOESIF_CACHE_I;
}

MOESIF_protocol::~MOESIF_protocol ()
{    
}

void MOESIF_protocol::dump (void)
{
  const char *block_states[9] = {"X","I","S","E","O","M","F"};
  if (state > (sizeof(block_states) / sizeof(char*)))
    {
      fprintf(stderr, "INVALID PRINT STATE\n");
    }
  else
    fprintf (stdout, "MOESIF_protocol - state: %s\n", block_states[state]);
}

void MOESIF_protocol::process_cache_request (Mreq *request)
{
  switch (state) {
  case MOESIF_CACHE_I:do_cache_I(request);break;
  case MOESIF_CACHE_E:do_cache_E(request);break;
  case MOESIF_CACHE_S:do_cache_S(request);break;
  case MOESIF_CACHE_O:do_cache_O(request);break;
  case MOESIF_CACHE_M:do_cache_M(request);break;
  case MOESIF_CACHE_F:do_cache_F(request);break;
  default:
    fatal_error ("Invalid Cache State for MOESIF Protocol\n");
  }
}

void MOESIF_protocol::process_snoop_request (Mreq *request)
{
  switch (state) {
  case MOESIF_CACHE_I:do_snoop_I(request);break;
  case MOESIF_CACHE_I_SE:do_snoop_I_SE(request);break;
  case MOESIF_CACHE_E:do_snoop_E(request);break;
  case MOESIF_CACHE_S:do_snoop_S(request);break;
  case MOESIF_CACHE_O:do_snoop_O(request);break;
  case MOESIF_CACHE_M:do_snoop_M(request);break;
  case MOESIF_CACHE_F:do_snoop_F(request);break;
  case MOESIF_CACHE_IM:do_snoop_IM(request);break;
  case MOESIF_CACHE_SM:do_snoop_SM(request);break;
  case MOESIF_CACHE_OM:do_snoop_OM(request);break;
  case MOESIF_CACHE_FM:do_snoop_FM(request);break;
  default:
    fatal_error ("Invalid Cache State for MOESIF Protocol\n");
  }
}

inline void MOESIF_protocol::do_cache_F (Mreq *request)
{
  switch(request->msg){
  case LOAD://a read miss!
    send_DATA_to_proc(request->addr);
    break;

  case STORE://a write miss!, waiting for data as permission; see do_snoop_FM
    send_GETM(request->addr);
    state = MOESIF_CACHE_FM;
    Sim->cache_misses ++;
    break;

  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: F state shouldn't see this message\n");
  }
}

inline void MOESIF_protocol::do_cache_I (Mreq *request)
{
  switch(request->msg){
  case LOAD://a read miss!
    send_GETS(request->addr);
    state = MOESIF_CACHE_I_SE;//waiting for data; when the snooper gets the data, the state is updated to S or E depending on the shared line:see do_snoop_I_SE
    Sim->cache_misses ++;
    break;

  case STORE://a write miss!
    send_GETM(request->addr);
    Sim->cache_misses ++;
    state = MOESIF_CACHE_IM;

    break;

  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: I state shouldn't see this message\n");
  }
}

inline void MOESIF_protocol::do_cache_S (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit,waiting for data as permission
    send_GETM(request->addr);
    Sim->cache_misses ++;
    state = MOESIF_CACHE_SM;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: S state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_cache_E (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit, silent upgrade to M
    send_DATA_to_proc(request->addr);
    Sim->silent_upgrades ++;
    state = MOESIF_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: E state shouldn't see this message\n");
  }
}

inline void MOESIF_protocol::do_cache_O (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit, waiting for data as permission see do_snoop_OM
    state = MOESIF_CACHE_OM;
    send_GETM(request->addr);
    Sim->cache_misses ++;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: O state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_cache_M (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE:
    send_DATA_to_proc(request->addr);
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: S state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_snoop_F (Mreq *request)
{
  switch(request->msg){
  case GETS://F intervenes on getS
    send_DATA_on_bus(request->addr,request->src_mid);
    set_shared_line();
    break;
  case GETM://intervene!
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MOESIF_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: E state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_snoop_I (Mreq *request)
{//no reaction to anything on bus
  switch(request->msg){
  case GETS:
  case GETM:
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: I state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_snoop_S (Mreq *request)
{
  switch(request->msg){
  case GETS:
    set_shared_line();
    break;
  case GETM:
    state = MOESIF_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: S state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_snoop_E (Mreq *request)
{
  switch(request->msg){
  case GETS://E->F and intervene
    send_DATA_on_bus(request->addr,request->src_mid);
    set_shared_line();
    state = MOESIF_CACHE_F;
    break;
  case GETM:
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MOESIF_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: E state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_snoop_O (Mreq *request)
{
  switch(request->msg){
  case GETS://intervene
    set_shared_line();
    send_DATA_on_bus(request->addr,request->src_mid);
    break;
  case GETM://intervene and invalidated
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MOESIF_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: O state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_snoop_M (Mreq *request)
{
  switch(request->msg){
  case GETS:
    send_DATA_on_bus(request->addr,request->src_mid);
    set_shared_line();
    state = MOESIF_CACHE_O;
    break;
  case GETM:
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MOESIF_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: M state shouldn't see this message\n");

  }
}


inline void MOESIF_protocol::do_snoop_I_SE (Mreq *request)
{
  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    
    if(get_shared_line()){
      state = MOESIF_CACHE_S;
    }
    else{
      state = MOESIF_CACHE_E;
    }
    send_DATA_to_proc(request->addr);
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: S state shouldn't see this message\n");

  }
}


inline void MOESIF_protocol::do_snoop_IM (Mreq *request)
{
  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MOESIF_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: IM state shouldn't see this message\n");

  }
}


inline void MOESIF_protocol::do_snoop_SM (Mreq *request)
{
  switch(request->msg){
  case GETS:
    set_shared_line();
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MOESIF_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: SM state shouldn't see this message\n");

  }
}

inline void MOESIF_protocol::do_snoop_OM (Mreq *request)
{
  switch(request->msg){
  case GETS://again intervene
    send_DATA_on_bus(request->addr,request->src_mid);
    set_shared_line();
    break;
  case GETM://intervene this time and no longer by transiting into SM
    send_DATA_on_bus(request->addr,request->src_mid);//this is the getM from itself
    state = MOESIF_CACHE_SM;
    break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MOESIF_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: OM state shouldn't see this message\n");

  }
}


inline void MOESIF_protocol::do_snoop_FM (Mreq *request)
{
  switch(request->msg){
  case GETS:
    send_DATA_on_bus(request->addr,request->src_mid);
    set_shared_line();
    break;
  case GETM://intervene this time and no longer by entering SM state
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MOESIF_CACHE_SM;
    break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MOESIF_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: OM state shouldn't see this message\n");

  }
}
