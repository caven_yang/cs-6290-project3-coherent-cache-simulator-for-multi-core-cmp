#include "MSI_protocol.h"
#include "../sim/mreq.h"
#include "../sim/sim.h"
#include "../sim/hash_table.h"

extern Simulator *Sim;

/*************************
 * Constructor/Destructor.
 *************************/
MSI_protocol::MSI_protocol (Hash_table *my_table, Hash_entry *my_entry)
  : Protocol (my_table, my_entry)
{
  this->state = MSI_CACHE_I;
}

MSI_protocol::~MSI_protocol ()
{    
}

void MSI_protocol::dump (void)
{
  const char *block_states[4] = {"X","I","S","M"};
  if (state > (sizeof(block_states) / sizeof(char*)))
    {
      fprintf(stderr, "INVALID PRINT STATE\n");
    }
  else
    fprintf (stdout, "MSI_protocol - state: %s\n", block_states[state]);
}

void MSI_protocol::process_cache_request (Mreq *request)
{
  switch (state) {
  case MSI_CACHE_I:do_cache_I(request);break;
  case MSI_CACHE_S:do_cache_S(request);break;  
  case MSI_CACHE_M:do_cache_M(request);break;
  default:
    fatal_error ("Invalid Cache State for MSI Protocol\n");
  }
}

void MSI_protocol::process_snoop_request (Mreq *request)
{
  switch (state) {

  case MSI_CACHE_IS: do_snoop_IS(request);break;
  case MSI_CACHE_IM: do_snoop_IM(request);break;
  case MSI_CACHE_I: do_snoop_I(request);break;

  case MSI_CACHE_SM: do_snoop_SM(request);break;
  case MSI_CACHE_S: do_snoop_S(request);break;
  case MSI_CACHE_M: do_snoop_M(request);break;
  default:
    fatal_error ("Invalid Cache State for MSI Protocol\n");
  }
}

inline void MSI_protocol::do_cache_I (Mreq *request)
{
  
  switch(request->msg){
  case LOAD://read miss!
    send_GETS(request->addr);
    state = MSI_CACHE_IS;//waiting for data; when the snooper gets the data, the state is updated to S; see do_snoop_IS
    Sim->cache_misses ++;
    break;

  case STORE://write miss!
    send_GETM(request->addr);
    Sim->cache_misses ++;
    state = MSI_CACHE_IM;//waiting for data, transits to M on receiving data. see do_snoop_IM
    break;

  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: I state shouldn't see this message\n");
  }

}

inline void MSI_protocol::do_cache_S (Mreq *request)
{
  
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit
    send_GETM(request->addr);
    Sim->cache_misses ++;
    state = MSI_CACHE_SM;//waiting for data( as permission); transits to M on receiving data. see do_snoop_SM
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: S state shouldn't see this message\n");

  }
  
}

inline void MSI_protocol::do_cache_M (Mreq *request)
{  
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit
    send_DATA_to_proc(request->addr);
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: M state shouldn't see this message\n");

  }
}


inline void MSI_protocol::do_snoop_I (Mreq *request)
{//in state I: don't react to anything on the bus
  switch(request->msg){
  case GETS:
  case GETM:
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: I state shouldn't see this message\n");

  }
}

inline void MSI_protocol::do_snoop_S (Mreq *request)
{
  switch(request->msg){
  case GETS:break;
  case GETM://invalidated
    state = MSI_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: S state shouldn't see this message\n");
  }
}

inline void MSI_protocol::do_snoop_M (Mreq *request)
{
  switch(request->msg){
  case GETS://intervene
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MSI_CACHE_S;
    break;
  case GETM://intervene and invalidated
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MSI_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: M state shouldn't see this message\n");
  }
}

inline void MSI_protocol::do_snoop_IS (Mreq *request)
{

  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MSI_CACHE_S;
    //no need to set the shared line in MSI protocol either
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: IS state shouldn't see this message\n");

  }
}


inline void MSI_protocol::do_snoop_SM (Mreq *request)
{

  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MSI_CACHE_M;
    //no need to set the shared line in MSI protocol either
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: SM state shouldn't see this message\n");

  }
}


inline void MSI_protocol::do_snoop_IM (Mreq *request)
{
  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MSI_CACHE_M;
    //no need to set the shared line in MSI protocol either
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: IM state shouldn't see this message\n");

  }
}
