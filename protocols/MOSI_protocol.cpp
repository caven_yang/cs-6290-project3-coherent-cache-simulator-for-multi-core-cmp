#include "MOSI_protocol.h"
#include "../sim/mreq.h"
#include "../sim/sim.h"
#include "../sim/hash_table.h"

extern Simulator *Sim;

/*************************
 * Constructor/Destructor.
 *************************/
MOSI_protocol::MOSI_protocol (Hash_table *my_table, Hash_entry *my_entry)
  : Protocol (my_table, my_entry)
{
  this->state = MOSI_CACHE_I;
}

MOSI_protocol::~MOSI_protocol ()
{    
}

void MOSI_protocol::dump (void)
{
  const char *block_states[5] = {"X","I","S","O","M"};
  if (state > (sizeof(block_states) / sizeof(char*)))
    {
      fprintf(stderr, "INVALID PRINT STATE\n");
    }
  else
    fprintf (stdout, "MOSI_protocol - state: %s\n", block_states[state]);
}

void MOSI_protocol::process_cache_request (Mreq *request)
{
  switch (state) {
  case MOSI_CACHE_I:do_cache_I(request);break;
  case MOSI_CACHE_S:do_cache_S(request);break;
  case MOSI_CACHE_M:do_cache_M(request);break;
  case MOSI_CACHE_O:do_cache_O(request);break;
  default:
    fatal_error ("Invalid Cache State for MOSI Protocol\n");
  }
}

void MOSI_protocol::process_snoop_request (Mreq *request)
{
  switch (state) {
  case MOSI_CACHE_I:do_snoop_I(request);break;
  case MOSI_CACHE_IS:do_snoop_IS(request);break;
  case MOSI_CACHE_IM:do_snoop_IM(request);break;
  case MOSI_CACHE_S:do_snoop_S(request);break;
  case MOSI_CACHE_SM:do_snoop_SM(request);break;    
  case MOSI_CACHE_M:do_snoop_M(request);break;
  case MOSI_CACHE_O:do_snoop_O(request);break;
  case MOSI_CACHE_OM:do_snoop_OM(request);break;
  default:
    fatal_error ("Invalid Cache State for MOSI Protocol\n");
  }
}

inline void MOSI_protocol::do_cache_I (Mreq *request)
{
  switch(request->msg){
  case LOAD://a read miss!
    send_GETS(request->addr);
    state = MOSI_CACHE_IS;//waiting for data; when the snooper gets the data, the state is updated to S; see do_snoop_IS
    Sim->cache_misses ++;
    break;

  case STORE://a write miss!
    send_GETM(request->addr);//waiting for data as permission; when the snooper gets the data, the state is updated to S; see do_snoop_IM
    Sim->cache_misses ++;
    state = MOSI_CACHE_IM;
    break;

  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: I state shouldn't see this message\n");
  }

}

inline void MOSI_protocol::do_cache_S (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit, waiting for data as permission; see do_snoop_SM
    send_GETM(request->addr);
    Sim->cache_misses ++;
    state = MOSI_CACHE_SM;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: S state shouldn't see this message\n");

  }
}

inline void MOSI_protocol::do_cache_O (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit, see do_snoop_OM
    state = MOSI_CACHE_OM;
    send_GETM(request->addr);
    Sim->cache_misses ++;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: O state shouldn't see this message\n");

  }
}

inline void MOSI_protocol::do_cache_M (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE:
    send_DATA_to_proc(request->addr);
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: S state shouldn't see this message\n");

  }
}

inline void MOSI_protocol::do_snoop_I (Mreq *request)
{//in state I: don't react to anything on the bus
  switch(request->msg){
  case GETS:
  case GETM:
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: I state shouldn't see this message\n");

  }
}

inline void MOSI_protocol::do_snoop_S (Mreq *request)
{
  switch(request->msg){
  case GETS:break;
  case GETM:
    state = MOSI_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: S state shouldn't see this message\n");

  }
}

inline void MOSI_protocol::do_snoop_O (Mreq *request)
{
  switch(request->msg){
  case GETS://intervene
    send_DATA_on_bus(request->addr,request->src_mid);
    break;
  case GETM://intervene and invalidated
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MOSI_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: O state shouldn't see this message\n");

  }
}

inline void MOSI_protocol::do_snoop_M (Mreq *request)
{
  switch(request->msg){
  case GETS://intervene
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MOSI_CACHE_O;
    break;
  case GETM://intervene and invalidated
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MOSI_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: M state shouldn't see this message\n");

  }
}

inline void MOSI_protocol::do_snoop_IS (Mreq *request)
{
  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MOSI_CACHE_S;
    //no need to set the shared line in MSI protocol either
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: IS state shouldn't see this message\n");

  }
}

inline void MOSI_protocol::do_snoop_IM (Mreq *request)
{
  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA://get the permission
    send_DATA_to_proc(request->addr);
    state = MOSI_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: IM state shouldn't see this message\n");

  }
}


inline void MOSI_protocol::do_snoop_SM (Mreq *request)
{
  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MOSI_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: SM state shouldn't see this message\n");

  }
}


inline void MOSI_protocol::do_snoop_OM (Mreq *request)
{
  switch(request->msg){
  case GETS://both cases: intervene once only and transits to SM( still waiting for permission but no longer intervene
  case GETM:
    send_DATA_on_bus(request->addr,request->src_mid);//this is the getM from itself
    state = MOSI_CACHE_SM;
    break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MOSI_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: OM state shouldn't see this message\n");

  }
}
