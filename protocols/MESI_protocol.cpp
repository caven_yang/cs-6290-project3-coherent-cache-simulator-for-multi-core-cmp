#include "MESI_protocol.h"
#include "../sim/mreq.h"
#include "../sim/sim.h"
#include "../sim/hash_table.h"

extern Simulator *Sim;

/*************************
 * Constructor/Destructor.
 *************************/
MESI_protocol::MESI_protocol (Hash_table *my_table, Hash_entry *my_entry)
  : Protocol (my_table, my_entry)
{
  this->state = MESI_CACHE_I;
}

MESI_protocol::~MESI_protocol ()
{    
}

void MESI_protocol::dump (void)
{
  const char *block_states[5] = {"X","I","S","E","M"};
  if (state > (sizeof(block_states) / sizeof(char*)))
    {
      fprintf(stderr, "INVALID PRINT STATE\n");
    }
  else
    fprintf (stdout, "MESI_protocol - state: %s\n", block_states[state]);
}

void MESI_protocol::process_cache_request (Mreq *request)
{
  switch (state) {
  case MESI_CACHE_I:do_cache_I(request);break;
  case MESI_CACHE_S:do_cache_S(request);break;
  case MESI_CACHE_M:do_cache_M(request);break;
  case MESI_CACHE_E:do_cache_E(request);break;
  default:
    fatal_error ("Invalid Cache State for MESI Protocol\n");
  }
}

void MESI_protocol::process_snoop_request (Mreq *request)
{
  switch (state) {
  case MESI_CACHE_I:do_snoop_I(request);break;
  case MESI_CACHE_S:do_snoop_S(request);break;
  case MESI_CACHE_M:do_snoop_M(request);break;
  case MESI_CACHE_E:do_snoop_E(request);break;
  case MESI_CACHE_I_SE:do_snoop_I_SE(request);break;
  case MESI_CACHE_IM:do_snoop_IM(request);break;
  case MESI_CACHE_SM:do_snoop_SM(request);break;
  default:
    fatal_error ("Invalid Cache State for MESI Protocol\n");
  }
}

inline void MESI_protocol::do_cache_I (Mreq *request)
{
  switch(request->msg){
  case LOAD://a read miss!
    send_GETS(request->addr);
    state = MESI_CACHE_I_SE;//waiting for data; when the snooper gets the data and shared line on, go to S state; when the data comes with shared line off, transits to E state; see do_snoop_I_SE
    Sim->cache_misses ++;
    break;

  case STORE://a write miss!
    send_GETM(request->addr);//waiting for data as permission
    Sim->cache_misses ++;
    state = MESI_CACHE_IM;
    break;

  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: I state shouldn't see this message\n");
  }
}

inline void MESI_protocol::do_cache_S (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit,waiting for data as permission; see do_snoop_SM
    send_GETM(request->addr);
    Sim->cache_misses ++;
    state = MESI_CACHE_SM;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: S state shouldn't see this message\n");

  }
}

inline void MESI_protocol::do_cache_E (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE://write hit,silent upgrade to M
    send_DATA_to_proc(request->addr);
    Sim->silent_upgrades ++;
    state = MESI_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: S state shouldn't see this message\n");

  }
}

inline void MESI_protocol::do_cache_M (Mreq *request)
{
  switch(request->msg){
  case LOAD://read hit
    send_DATA_to_proc(request->addr);
    break;
  case STORE:
    send_DATA_to_proc(request->addr);
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("Client: S state shouldn't see this message\n");

  }
}

inline void MESI_protocol::do_snoop_I (Mreq *request)
{//no reaction to anything on bus;
  switch(request->msg){
  case GETS:
  case GETM:
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: I state shouldn't see this message\n");

  }
}

inline void MESI_protocol::do_snoop_S (Mreq *request)
{
  switch(request->msg){
  case GETS:
    set_shared_line();
    break;
  case GETM://invalidated, MEM sends data
    state = MESI_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: S state shouldn't see this message\n");

  }
}

inline void MESI_protocol::do_snoop_E (Mreq *request)
{  
  switch(request->msg){
  case GETS://intervene,
    send_DATA_on_bus(request->addr,request->src_mid);
    set_shared_line();
    state = MESI_CACHE_S;
    break;
  case GETM://intervene, invalidated
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MESI_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: M state shouldn't see this message\n");

  }
}

inline void MESI_protocol::do_snoop_M (Mreq *request)
{
  switch(request->msg){
  case GETS:
    send_DATA_on_bus(request->addr,request->src_mid);
    set_shared_line();
    state = MESI_CACHE_S;
    break;
  case GETM:
    send_DATA_on_bus(request->addr,request->src_mid);
    state = MESI_CACHE_I;
    break;
  case DATA:
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: M state shouldn't see this message\n");

  }

}

inline void MESI_protocol::do_snoop_I_SE (Mreq *request)
{
  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    
    if(get_shared_line()){
      state = MESI_CACHE_S;
    }
    else{
      state = MESI_CACHE_E;
    }
    send_DATA_to_proc(request->addr);
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: S state shouldn't see this message\n");

  }
}

inline void MESI_protocol::do_snoop_IM (Mreq *request)
{
  switch(request->msg){
  case GETS:
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MESI_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: IM state shouldn't see this message\n");

  }
}


inline void MESI_protocol::do_snoop_SM (Mreq *request)
{
  switch(request->msg){
  case GETS:
    set_shared_line();
  case GETM:break;
  case DATA:
    send_DATA_to_proc(request->addr);
    state = MESI_CACHE_M;
    break;
  default:
    request->print_msg (my_table->moduleID, "ERROR");
    fatal_error ("snoop: SM state shouldn't see this message\n");

  }
}
