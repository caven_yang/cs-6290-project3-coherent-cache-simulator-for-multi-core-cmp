filedir='../traces/experiment';

filedir=strcat(filedir,id);


filename=strcat(filedir,'/config');


fid=fopen(filename);

line=fgets(fid);

data=textscan(line,'%d');

config=data{1};

fclose(fid);

rd_trace=cell(config,1);%cell, the ith component is the read traces for processor i;
wr_trace=cell(config,1);
total=[];
    


cache_line_size=64; %this is found in sim/node.cpp!

for i=0:(config-1)
   filename=strcat(filedir,'/p');
   filename=strcat(filename,num2str(i));
   filename=strcat(filename,'.trace');
   fid=fopen(filename,'r');
   
   
   while(true)
       
       line=fgets(fid); 
       if(~ischar(line))
           break;
       end
       
       data=textscan(line,'%s 0x%s');
       str_addr=data{2};
       
       addr=floor(hex2dec(str_addr)/64);
       
       type=data{1};
       
       total=[total,addr];
       if(strcmp(type,'r'))%read
           rd_trace{i+1}=[rd_trace{i+1},addr];
          
           
       else%write
           wr_trace{i+1}=[wr_trace{i+1},addr];
           
           
       end
       
   end
   
   
   
   fclose(fid); 
    
    
end


addr_list=unique(total);%address list, each member is unique
rd_count=zeros(config,numel(addr_list)); %a matrix, the (i,j) component means how many times the jth address in addr_list is read from by
                                         % the ith processor
wr_count=zeros(config,numel(addr_list));
rd_uniq=zeros(config,1);%the ith component means how many different addresses the ith processor reads from
wr_uniq=zeros(config,1);

for i=1:config
   rtr=rd_trace{i};
   wtr=wr_trace{i};
    
   for j=1:numel(addr_list)
       sel_addr=addr_list(j);%selected address
       rd_count(i,j)=length(find(rtr==sel_addr));%how many times is the seleted address read from?
       wr_count(i,j)=length(find(wtr==sel_addr));
   end
    
   rd_uniq(i)=numel(unique(rtr));
   wr_uniq(i)=numel(unique(wtr));
    
end




%%%%%%%%%%%%%%%%%%%%Fig.2 series plotting%%%%%%%%%%%%%%%%%%%%%%%%%
% subplot(2,1,1);
% bar(rd_count,0.2,'stack');
% str='Experiment.';
% str=strcat(str,id);
% tit=strcat(str,':reads');
% title(tit);
% ylabel('count');
% xlabel('processor');
% subplot(2,1,2);
% bar(wr_count,0.2,'stack');
% title('writes');
% tit=strcat(str,':writes');
% title(tit);
% ylabel('count');
% str='processor';
% xlabel(str);
%%%%%%%%%%%%%%%%%%%%Fig.2 series plotting%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%Fig.3 series plotting%%%%%%%%%%%%%%%%%%%%%%%%%
bar([rd_uniq wr_uniq]);
xlabel('processor');
ylabel('different addresses accessed');
str='Experiment.';
str=strcat(str,id);
str=strcat(str,':');
str=strcat(str, num2str(numel(addr_list)));
str=strcat(str,' different addresses in total');
title(str);

leg=char('rd','wr');
legend(leg);
%%%%%%%%%%%%%%%%%%%%Fig.3 series plotting%%%%%%%%%%%%%%%%%%%%%%%%%