

filename='../log/log_';

filename=strcat(filename,id);

fid=fopen(filename,'r');

protocol=[];
run_time=[];
misses=[];
access=[];
sil_upgrades=[];
transfers=[];

while(true)
   line=fgets(fid);
   if(~ischar(line))
       break;
   end
   
   stat=textscan(line,'%s :');
   protocol=[protocol,stat{1}];
   
%    fprintf('%s\n',line);
   line=fgets(fid);
   
   stat=textscan(line,'Run Time:\t%d cycles');
   run_time=[run_time,stat{1}];
   
   line=fgets(fid);
   stat=textscan(line,'Cache Misses:\t%d misses');
   misses=[misses,stat{1}];
   
   line=fgets(fid);
   stat=textscan(line,'Cache Accesses:\t%d accesses');
   access=[access,stat{1}];
    
    
   line=fgets(fid);
   stat=textscan(line,'Silent Upgrades:\t%d upgrades');
   sil_upgrades=[sil_upgrades,stat{1}];
   
   
   line=fgets(fid);
   stat=textscan(line,'$-to-$ Transfers:\t%d transfers');
   transfers=[transfers,stat{1}];

end

subplot(2,1,1);




b1=bar(run_time,'FaceColor','red','BarWidth',0.2);
set(gca,'XTickLabel',protocol);
legend('run time');
ylabel('cycles','FontSize',30);

grid on;
str='Statistics for Experiment.';
str=strcat(str,id);
title(str);

subplot(2,1,2);
bar([misses', sil_upgrades', transfers']);
set(gca,'XTickLabel',protocol,'FontSize',24);
grid on;

leg=char('misses', 'silent upgrades', '$-$ transfers');
legend(leg);

str='accesses:';
str=strcat(str,num2str(access(1)));
title(str);




